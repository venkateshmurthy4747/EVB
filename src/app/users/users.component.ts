import { Component } from '@angular/core';
import { Chart } from 'chart.js';
import { APIService } from '../api.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent{
	constructor(private apiservice:APIService) { }
	title = 'app';
	func:any='2';
	toggle:any;
	v:any;
	 users:any;
	 ngOnInit() {
		console.log('this.func')
	 }
  myChart = new Chart("myChart", {
    
	type: 'line',
   
	data: {
	  
		labels:['Jun 15 2022 18:30:00','Jun 16 2022','Jun 17 2022','Jun 18 2022'],
		datasets: [
{

label: 'Charging',
data:[50,20,30,40],

backgroundColor: [
	'rgba(255, 99, 132, 0.2)',
	'rgba(54, 162, 235, 0.2)',
	'rgba(255, 206, 86, 0.2)',
	'rgba(75, 192, 192, 0.2)',
	'rgba(153, 102, 255, 0.2)',
	'rgba(255, 159, 64, 0.2)'
],
borderColor: [
	'rgba(255, 99, 132, 1)',
	'rgba(54, 162, 235, 1)',
	'rgba(255, 206, 86, 1)',
	'rgba(75, 192, 192, 1)',
	'rgba(153, 102, 255, 1)',
	'rgba(255, 159, 64, 1)'
],
borderWidth: 1
},
// {

//   label: 'Discharging',
//   data: recuserId,

//   backgroundColor: [
//       'rgba(255, 99, 132, 0.2)',
//       'rgba(54, 162, 235, 0.2)',
//       'rgba(255, 206, 86, 0.2)',
//       'rgba(75, 192, 192, 0.2)',
//       'rgba(153, 102, 255, 0.2)',
//       'rgba(255, 159, 64, 0.2)'
//   ],
//   borderColor: [
//       'rgba(255, 99, 132, 1)',
//       'rgba(54, 162, 235, 1)',
//       'rgba(255, 206, 86, 1)',
//       'rgba(75, 192, 192, 1)',
//       'rgba(153, 102, 255, 1)',
//       'rgba(255, 159, 64, 1)'
//   ],
//   borderWidth: 1
// },
// {

//   label: 'Voltage 1',
//   data: [34, 22, 45, 67, 89, 12, 45],

//   backgroundColor: [
//       'rgba(255, 99, 132, 0.2)',
//       'rgba(54, 162, 235, 0.2)',
//       'rgba(255, 206, 86, 0.2)',
//       'rgba(75, 192, 192, 0.2)',
//       'rgba(153, 102, 255, 0.2)',
//       'rgba(255, 159, 64, 0.2)'
//   ],
//   borderColor: [
//       'rgba(255, 99, 132, 1)',
//       'rgba(54, 162, 235, 1)',
//       'rgba(255, 206, 86, 1)',
//       'rgba(75, 192, 192, 1)',
//       'rgba(153, 102, 255, 1)',
//       'rgba(255, 159, 64, 1)'
//   ],
//   borderWidth: 1
// },{

//   label: 'Voltage 2',
//   data: [23, 12, 45, 67, 89, 12, 45],

//   backgroundColor: [
//       'rgba(255, 99, 132, 0.2)',
//       'rgba(54, 162, 235, 0.2)',
//       'rgba(255, 206, 86, 0.2)',
//       'rgba(75, 192, 192, 0.2)',
//       'rgba(153, 102, 255, 0.2)',
//       'rgba(255, 159, 64, 0.2)'
//   ],
//   borderColor: [
//       'rgba(255, 99, 132, 1)',
//       'rgba(54, 162, 235, 1)',
//       'rgba(255, 206, 86, 1)',
//       'rgba(75, 192, 192, 1)',
//       'rgba(153, 102, 255, 1)',
//       'rgba(255, 159, 64, 1)'
//   ],
//   borderWidth: 1
// },
// {

//   label: 'Voltage 3',
//   data: [27, 12, 45, 67, 89, 12, 45],

//   backgroundColor: [
//       'rgba(255, 99, 132, 0.2)',
//       'rgba(54, 162, 235, 0.2)',
//       'rgba(255, 206, 86, 0.2)',
//       'rgba(75, 192, 192, 0.2)',
//       'rgba(153, 102, 255, 0.2)',
//       'rgba(255, 159, 64, 0.2)'
//   ],
//   borderColor: [
//       'rgba(255, 99, 132, 1)',
//       'rgba(54, 162, 235, 1)',
//       'rgba(255, 206, 86, 1)',
//       'rgba(75, 192, 192, 1)',
//       'rgba(153, 102, 255, 1)',
//       'rgba(255, 159, 64, 1)'
//   ],
//   borderWidth: 1
// },
// {

//   label: 'Voltage 4',
//   data: [{x: new Date(),
//     y: 1},{
//       t: new Date(),
//       y: 10
//   }],

//   backgroundColor: [
//       'rgba(255, 99, 132, 0.2)',
//       'rgba(54, 162, 235, 0.2)',
//       'rgba(255, 206, 86, 0.2)',
//       'rgba(75, 192, 192, 0.2)',
//       'rgba(153, 102, 255, 0.2)',
//       'rgba(255, 159, 64, 0.2)'
//   ],
//   borderColor: [
//       'rgba(255, 99, 132, 1)',
//       'rgba(54, 162, 235, 1)',
//       'rgba(255, 206, 86, 1)',
//       'rgba(75, 192, 192, 1)',
//       'rgba(153, 102, 255, 1)',
//       'rgba(255, 159, 64, 1)'
//   ],
//   borderWidth: 1
// },
]

	},
	options: {
	  
	  
		scales: {
			y: {
			  title:{
				display:true,
				text:'Value',
				font:{
				  size:30
				},
				color:'blue',},
				beginAtZero: true,
				max:60,
				ticks:{
				  color:'black',
				  font:{
					size:13
				  }
				 }
			   
			},
			x:{
			  title:{
				display:true,
				text:'Time',
				font:{
				  size:30
				},
				color:'blue'
			  },
			  min: Date.now()-(60*1000*60*24*60),
			max:Date.now(),
			 type: 'time',
			 ticks:{
			  color:'black',
			  font:{
				size:13
			  }
			 }
			 
			},
			
		} ,
		plugins: {
		 
		  zoom: {
			pan:{
			  enabled:true,
			  mode:'x',
			  
			},
			zoom: {
			  
			  wheel: {
				enabled: true,
				speed:0.02,
				
			   
			  },
			  
			  drag: {
				enabled: true,
				modifierKey: 'alt',
				
			  },
			  mode: 'x',
			}
		  }
		}},
	  
		
	  },
		);
		
		
}