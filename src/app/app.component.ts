import { Component, OnInit } from '@angular/core';
import { Chart, registerables, Scale } from 'chart.js';
import zoomPlugin from 'chartjs-plugin-zoom';
import 'chartjs-adapter-moment';
import { APIService } from './api.service';
import { filter } from 'rxjs';




Chart.register(zoomPlugin);
Chart.register(...registerables);
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  
  constructor(private apiservice:APIService) { }
  title = 'app';
 func:any='2';
 toggle:any;
 v:any;
 myChart:any;
  users:any;
 
  ngOnInit() {
  
     
//  console.log(a)
    this.apiservice.getAllData().subscribe(data=>{
     
    this.users=data;
   const reccharging=[];
   const recdischarging=[];
   const recDate=[];
   const recTime=[];
   const recv2=[];
   const recv1=[];
   const recv3=[];
   const recv4=[];
  
    // console.log(this.users.Date)
    for(let user in this.users.data){
      var jsonDate=this.users.data[user].Date;
      var jsonTime=this.users.data[user].Time;
      var jsoncharging=this.users.data[user].CA
      var jsonDischarging=this.users.data[user].DA
      var jsonv1=this.users.data[user].v1
      var jsonv2=this.users.data[user].v2
      var jsonv3=this.users.data[user].v3
      var jsonv4=this.users.data[user].v4
      // console.log(this.users.Date);
      // recDate.push(jsonDate);
      // recTime.push(jsonTime);
      reccharging.push(jsoncharging);
      recdischarging.push(jsonDischarging);
      recv1.push(jsonv1)
      recv2.push(jsonv2)
      recv3.push(jsonv3)
      recv4.push(jsonv4)
      var combine=jsonDate.concat(jsonTime)
recDate.push(combine)
    }
    
    console.log(jsonv1);
   
     this.myChart = new Chart("myChart", {
    
      type: 'line',
     
      data: {
        
          labels:recDate,
          datasets: [
{
  
  label: 'Charging',
  data:reccharging,
 
  backgroundColor: [
      'rgba(255, 99, 132, 0.2)',
      
  ],
  borderColor: [
      'rgba(255, 99, 132, 1)',
      
  ],
  borderWidth: 2,
  pointRadius:1,
  tension:0.2,
  yAxisID: 'y',
},
{
  
  label: 'Discharging',
  data: recdischarging,
  
  backgroundColor: [
     
      'rgba(54, 162, 235, 0.2)',
    
  ],
  tension:0.2,
  borderColor: [
    
      'rgba(54, 162, 235, 1)',
      
  ],
  borderWidth: 2,
  pointRadius:1,
  yAxisID: 'y',
},
{
  
  label: 'Voltage 1',
  data: recv1,
  
  backgroundColor: [
     
      'rgba(255, 206, 86, 0.2)',
      
  ],
  borderColor: [
     
      'rgba(255, 206, 86, 1)',
     
  ],
  borderWidth: 2,
  pointRadius:1,
  yAxisID: 'y1',
},{
  
  label: 'Voltage 2',
  data: recv2,
  
  backgroundColor: [
     
      'rgba(75, 192, 192, 0.2)',
      
  ],
  borderColor: [
     
      'rgba(75, 192, 192, 1)',
     
  ],
  borderWidth: 2,
  pointRadius:1,
  yAxisID: 'y1',
},
{
  
  label: 'Voltage 3',
  data: recv3,
  
  backgroundColor: [
      
      'rgba(153, 102, 255, 0.2)',
      
  ],
  borderColor: [
      
      'rgba(153, 102, 255, 1)',
     
  ],
  borderWidth: 2,
  pointRadius:1,
  yAxisID: 'y1',
},
{
  
  label: 'Voltage 4',
  data: recv4,
  
  backgroundColor: [
     
      'rgba(255, 159, 64, 0.2)'
  ],
  borderColor: [
     
      'rgba(255, 159, 64, 1)'
  ],
  borderWidth: 2,
  pointRadius:1,
  
  yAxisID: 'y1',
  
},
]

      },
      options: {
        
        
          scales: {
            
              y: {
                
                title:{
                  display:true,
                  text:'Current(Amp)',
                  font:{
                    size:30
                  },
                  color:'blue',},
                  beginAtZero: true,
                  min:-50,
                  max:50,
                  ticks:{
                    color:'black',
                   stepSize:10,
                    font:{
                      size:13
                    }
                   }
                 
              },
              y1:{
                position:'right',
                title:{
                  display:true,
                  text:'Voltage (V)',
                  font:{
                    size:30
                  },
                  color:'blue',},
                 
                   beginAtZero:true,
                  max:25,
                 min:-25,
                  ticks:{
                    
                    color:'black',
                    font:{
                      size:13
                    }
                   },
                   
              },
              x:{
                type: 'timeseries',
                time:{
                  parser:'DD-MM-YYYYhh:mm:ss',
                  displayFormats: {
                    DateTime: 'DD-MM-YYYY HH:MM:SS'
                  }
                 },
               
                min: Date.now()-(60*1000*60),
              max:Date.now(),
              title:{
                display:true,
                text:'Time',
                font:{
                  size:30
                },
                color:'blue'
              },
               
               ticks:{
                color:'black',
                font:{
                  size:13
                }
               }
               
              },
              
          } ,
          plugins: {
           legend:{position:'bottom',
           maxHeight:50,
          },
            zoom: {
              pan:{
                enabled:true,
                mode:'x',
                
              },
              zoom: {
                
                wheel: {
                  enabled: true,
                  speed:0.02,
                  
                 
                },
                
                drag: {
                  enabled: true,
                  modifierKey: 'alt',
                  
                },
                mode: 'x',
              }
            }
          }},
        
          
        },
          );
          this.myChart.update;
        
          
             })}
            
             resetZoomChart() {
              this.myChart.resetZoom();
            }
            zoombutton(value: any) {
              this.myChart.zoom(value);
            }
}